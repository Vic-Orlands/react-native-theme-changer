import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { ThemeProvider } from './context/ThemeContext';
import Home from './src/Home';

export default function App() {
  return (
    <ThemeProvider>
      <StatusBar />
      <Home />
    </ThemeProvider>
  );
}
